package io.fabric8.quickstarts.camel.endpoints;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

//@Component
public class HelloWorldEndPoint extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("timer://foo?period=5000")
                .setBody().constant("Hello World")
                .log(">>> ${body}");
    }

}
