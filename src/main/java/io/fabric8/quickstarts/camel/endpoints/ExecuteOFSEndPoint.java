package io.fabric8.quickstarts.camel.endpoints;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

@Component
public class ExecuteOFSEndPoint extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        restConfiguration().component("servlet").bindingMode(RestBindingMode.auto);


        rest("/ofs")
                .post("/execute")
                .to("direct:executeOfs");

        from("direct:executeOfs")
                .log(">>> ${body}")
                .to("mq:queue:T24.BZ.INT.OFS.ENQ.JSON.QUEUE?exchangePattern=InOut&replyTo=T24.BZ.INT.OFS.ENQ.JSON.REPLY.QUEUE&requestTimeout=1000000")
                .log("${body}");
    }
}
